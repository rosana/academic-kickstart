+++
# Experience widget.
widget = "experience"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Experience"
subtitle = ""

# Order that this section will appear in.
weight = 8

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "January 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

#[[experience]]
#  title = "CEO"
#  company = "GenCoin"
#  company_url = ""
#  location = "California"
#  date_start = "2017-01-01"
#  date_end = ""
#  description = """
#  Responsibilities include:

#  * Analysing
#  * Modelling
#  * Deploying
#  """

[[experience]]
    title = "Assistant Professor"
    company = "Universidad Nacional de Luján"
    company_url = ""
    location = "Buenos Aires"
    date_start = "2019-05-01"
    date_end = ""
    description = """Teach algorithms and data structures."""

[[experience]]
    title = "Associate Professor"
    company = "Universidad de Belgrano"
    company_url = ""
    location = "Buenos Aires"
    date_start = "2018-11-01"
    date_end = ""
    description = """Teach algorithms and data structures."""

[[experience]]
  title = "Adjunct Professor"
  company = "Universidad de Belgrano"
  company_url = ""
  location = "Buenos Aires"
  date_start = "2018-08-01"
  date_end = "2018-10-31"
  description = """Teach algorithms and data structures."""

[[experience]]
    title = "Head Teaching Assistant"
    company = "Universidad de Buenos Aires"
    company_url = ""
    location = "Buenos Aires"
    date_start = "2012-03-01"
    date_end = "2018-07-31"
    description = """Taught computer science courses and researched artificial neural networks."""
+++
