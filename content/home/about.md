+++
# About/Biography widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Artificial Intelligence",
    "Artificial Neural Networks",
    "Computer Vision"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "PhD in Computer Science"
  institution = "Universidad de Buenos Aires"
  year = 2008

[[education.courses]]
  course = "MSc in Computer Science"
  institution = "Universidad de Buenos Aires"
  year = 2001

[[education.courses]]
  course = "BSc in Computer Science"
  institution = "Universidad de Buenos Aires"
  year = 1998

+++

# Biography

Rosana Matuk is a professor of Computer Engineering at the Departamento de Ciencias Básicas, Universidad Nacional de Luján, Argentina. Her research interests include artificial neural networks, artificial intelligence and programmable matter.
