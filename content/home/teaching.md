+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "custom"
active = true
date = 2016-04-20T00:00:00

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Teaching"
subtitle = ""

# Order that this section will appear in.
weight = 60

#+++

#This is an example of using the *custom* widget to create your own homepage section.

#To remove this section, either delete `content/home/teaching.md` or edit the frontmatter of the file to deactivate the widget by setting `active = false`.

+++

I am teaching instructor for the following course at the Departamento de Ciencias Básicas, Universidad Nacional de Luján:

- Algorithms and data structures

I was teaching instructor for the following course at the Faculty of Computer Engineering, Universidad de Belgrano:

- Algorithms and data structures

I was a teaching assistant instructor for the following courses at the Department of Computer Science, Universidad de Buenos Aires:

- Artificial neural networks
- Deep learning
- Database systems
- Programming languages
- Algorithms and data structures
- Numerical methods
