+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "custom"
active = true
date = 2016-04-20T00:00:00

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Upcoming Conferences"
subtitle = ""

# Order that this section will appear in.
weight = 19

#+++

#This is an example of using the *custom* widget to create your own homepage section.

#To remove this section, either delete `content/home/teaching.md` or edit the frontmatter of the file to deactivate the widget by setting `active = false`.

+++

I am Program Committee Member of the following conference:

- CIARP 2019 (24th Iberoamerican Congress on Pattern Recognition), Havana, Cuba, October 2019: https://ciarp.uci.cu

Please consider submitting your work to CIARP 2019 !
